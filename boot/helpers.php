<?php

if(! function_exists('redirect')){
    function redirect ($uri, $status = '302', $headers = []){
        return new \Zend\Diactoros\Response\RedirectResponse($uri, $status, $headers);
    }
}