<?php

# PagesController routes
$route->get('/', '\App\Controller\PagesController::home')->setName('homepage');
$route->get('/draft', '\App\Controller\PagesController::draft')->setName('draft');
$route->get('/documentation', '\App\Controller\PagesController::documentation')->setName('documentation');

