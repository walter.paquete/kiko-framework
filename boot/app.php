<?php

include __DIR__.'/../vendor/autoload.php';

include __DIR__ . '/database.php';

# define container
$container = new League\Container\Container;

# allowing autowiring
$container->delegate(
    new League\Container\ReflectionContainer
);

# loading Services Providers
$container->addServiceProvider(App\Provider\AppServiceProvider::class);
$container->addServiceProvider(App\Provider\ViewServiceProvider::class);

# loading web routes
$route = $container->get(League\Route\RouteCollection::class);
include __DIR__ . '/routing.php';

# loading dispatching
$response = $route->dispatch($container->get('request'), $container->get('response'));

