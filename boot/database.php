<?php

#Databse configuration

$settings = array(
    'driver'    => 'mysql',
    'host'      => '127.0.0.1',
    'database'  => 'just_test',
    'username'  => 'root',
    'password'  => '',
    'collation' => 'utf8_general_ci',
    'charset'   => 'utf8',
    'prefix'    => '',
);

$factory = new \Illuminate\Database\Connectors\ConnectionFactory(new Illuminate\Container\Container);
$connection = $factory->make($settings);

$resolver = new \Illuminate\Database\ConnectionResolver();
$resolver->addConnection('default', $connection);
$resolver->setDefaultConnection('default');
\Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);
