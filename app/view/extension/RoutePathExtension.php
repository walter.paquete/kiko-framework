<?php

namespace App\View\Extension;

use League\Route\RouteCollection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RoutePathExtension extends AbstractExtension
{
    protected $routingService;

    public function __construct(RouteCollection $routeCollection)
    {
        $this->routingService = $routeCollection;
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('path', array($this, '_path')),
        );
    }

    public function _path($routeName)
    {
        return $this->routingService->getNamedRoute($routeName)->getPath();
    }

}
