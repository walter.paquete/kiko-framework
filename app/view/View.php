<?php

namespace App\View;

use Psr\Http\Message\ResponseInterface;
use Twig_Environment;

class View
{
    protected $twig;

    public function __construct( Twig_Environment $twig_Environment)
    {
        $this->twig = $twig_Environment;
    }

    public function render (ResponseInterface $response, $template, $data = []){
        $response->getBody()->write(
            $this->twig->render($template, $data)
        );

        return $response;
    }

}