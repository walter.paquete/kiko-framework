<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Model;

/** Model class example */
class Office extends Model
{
    public $table = "offices";

    public $timestamps = false;
}

