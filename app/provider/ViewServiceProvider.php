<?php

namespace App\Provider;

use App\View\Extension\RoutePathExtension;
use App\View\View;
use League\Container\ServiceProvider\AbstractServiceProvider;
use League\Route\RouteCollection;
use Twig_Loader_Filesystem;
use Twig_Environment;
use Twig_Extension_Debug;

class ViewServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        View::class
    ];

    public function register()
    {
        $container = $this->getContainer();

        $container->share(View::class, function () use ($container) {
            $loader = new Twig_Loader_Filesystem(__DIR__ . '/../view/template');
            $twig = new Twig_Environment($loader, array(
                'cache' => false,
                'debug' => true,
            ));
            $twig->addExtension(new Twig_Extension_Debug());
            $twig->addExtension(new \Twig_Extensions_Extension_Text());
            $twig->addExtension(new RoutePathExtension( $container->get(RouteCollection::class)));

            return new View($twig);
        });

    }
}