<?php

namespace App\Controller;

use App\View\View;

abstract class Controller
{
    protected $view;

    public function __construct(View $view){
        $this->view = $view;
    }

}