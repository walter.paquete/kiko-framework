<?php

namespace App\Controller;

use App\Model\Office;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/** Controller class example */
class PagesController extends Controller
{
    public function home(ServerRequestInterface $request, ResponseInterface $response){

        return $this->view->render($response, 'home.html.twig');
    }


    public function documentation(){

        return redirect('https://gitlab.com/walter.paquete/kiko-framework');
    }


    public function draft (ServerRequestInterface $request, ResponseInterface $response){
        // just to test : retrieving data (uncomment to test)
        // dd(Office::all());

        return $this->view->render($response, 'draft.html.twig', compact('offices'));
    }
}