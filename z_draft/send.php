<?php

/**
 * script just to test Mailjet API php
 */

require __DIR__.'/vendor/autoload.php';

$mj = new Mailjet(getenv('mailjet_api_user'), getenv('mailjet_api_pass'));

$msgHtml = "<html><h1>You mean it controls your actions?</h1>
<p>I care. So, what do you think of her, Han? I have traced the Rebel spies to her. Now she is my only link to finding their secret base. Remember, a Jedi can feel the Force flowing through him. I find your lack of faith disturbing.</p>
<p>I call it luck. Don't act so surprised, Your Highness. <strong> You weren't on any mercy mission this time.</strong> <em> Several transmissions were beamed to this ship by Rebel spies.</em> I want to know what happened to the plans they sent you.</p>
<h2>Ye-ha!</h2>
<p>Don't be too proud of this technological terror you've constructed. The ability to destroy a planet is insignificant next to the power of the Force. Don't be too proud of this technological terror you've constructed. The ability to destroy a planet is insignificant next to the power of the Force.</p>
<ol>
<li>All right. Well, take care of yourself, Han. I guess that's what you're best at, ain't it?</li><li>What!?</li><li>I don't know what you're talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan--</li>
</ol>

<h3>But with the blast shield down, I can't even see! How am I supposed to fight?</h3>
<p>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you're going. I don't know what you're talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan--</p>
<ul>
<li>You're all clear, kid. Let's blow this thing and go home!</li><li>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander. There'll be no one to stop us this time!</li><li>Hey, Luke! May the Force be with you.</li>
</ul></html>";


    $params = array(
        "method" => "POST",
        "from" => "toto@gmail.com",
        "to" => "toto@gmail.com",
        "subject" => "Testin mailjet API php",
        "html" => $msgHtml
    );

    $result = $mj->sendEmail($params);

    if ($mj->_response_code == 200){
        echo "success - email sent";
    }else{
        echo "error - ".$mj->_response_code;
        var_dump($result);
    }



