KiKo, PHP homemade framework

Goal : understand how php library work together.

Main composition
- Container system : based on "the php league" container
- Routing system : based on "the php league" routing
- Templating system : Symfony Twig
- ORM system : Laravel eloquent

To test : clone it first and then run php standalone server like this
Make : <code> composer install </code>
<code> php -S localhost:8080 -t public </code>

Enjoy :)